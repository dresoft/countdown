#include "receiver.h"
#include "diccionario.h"
#include "algoritmos.h"
#include <QTime>
#include <QDebug>
#include <QGuiApplication>
#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>

#define ARCHIVO_RECORDS "records.txt"
#define ARCHIVO_VACIO "---;---;---;\n0;0;0;"

Receiver::Receiver(QObject *parent) :
    QObject(parent)
{

}

//Comprueba si existe una palabra dentro del diccionario
bool Receiver::existePalabra(QString palabra)
{
    Diccionario *diccionario = Diccionario::getInstance();
    return diccionario->existePalabra(palabra);
}

QString Receiver::buscaPalabra()
{
    QTime time;
    time.start();

    QString resultado;
    Diccionario *diccionario = Diccionario::getInstance();
    resultado = diccionario->buscarPalabraMasLarga(letras_.toLower()).toUpper();
    qDebug() << "Receiver::buscaPalabra(" << letras_ << ") resultado = " << resultado
             << " en " << time.elapsed() << " ms";
    return resultado;
}

//Busca el exacto y muestra las operaciones realizadas. Devuelve el numero mas cercano encontrado
int Receiver::buscarExacto()
{
    QPair <int,QStringList> salida;
    //Lista
    Algoritmos *algoritmos = new Algoritmos(this);
    salida = algoritmos->ejecutarBusqueda(numeros_,exacto_);

    setValorCerebro(QString::number(salida.first));
    listaOperaciones_ = pintarNumeros(salida.second);
//    qDebug() << "NUMERO MAS CERCANO A " << exacto_ << " ENCONTRADO => " << valorLogradoCerebro_;
//    qDebug() << "OPERACIONES Realizadas: \n" << salida.second;

    delete(algoritmos);
    return salida.first;
}

//Ponemos en negrita los numeros que eran cifras originales dentro de las operaciones
QStringList Receiver::pintarNumeros(QStringList lista)
{
    QStringList salida = lista;

    for (int k=0;k<salida.count();k++)
        salida[k] = QString(" ").append(salida.at(k));

//    qDebug() << "LISTA ANTES = " << salida;
    for (int i=0;i<salida.count();i++)
    {
        QString aux = salida.at(i);
        //Si la operacion contiene un numero inicial, lo ponemos en negrita
        for (int j=0;j<listaNumeros_.count();j++)
        {
            if (aux.contains(listaNumeros_.at(j)))
            {
                QString inicial = listaNumeros_.at(j);
                inicial.insert(0," ");
                inicial.append(" ");
                QString final = listaNumeros_.at(j);
                final.insert(0," ");
                final.append(" ");
                final.insert(0,"<b>");
                final.append("</b>");
                aux.replace(inicial,final);
                salida[i] = aux;
            }
        }
    }
//    qDebug() << "LISTA DESPUES = " << salida;

    QString last = salida.last();
//    qDebug() << "LAST ANTES = " << last;
    if (last.contains(QString::number(exacto_)))
    {
        QString ini = QString::number(exacto_);
        ini.insert(0," ");
        QString fin = QString::number(exacto_);
        fin.insert(0," <b><font color=\"red\">");
        fin.append("</font></b>");
        last.replace(ini,fin);
        salida[salida.count()-1] = last;
//        qDebug() << "LAST DESPUES = " << last;
    }

    return salida;
}

void Receiver::setExacto(int valor)
{
    exacto_ = valor;
    QTime t;
    t.start();
    int resultado = buscarExacto();
    qDebug() << "= buscarExacto(" << valor << ") = " << resultado << " en " << t.elapsed() << " ms";
    qDebug() << "============================================================================================\n";
}

void Receiver::addCifra(int valor)
{
    if (numeros_.length() < 6)
    {
        numeros_ << valor;
        listaNumeros_ << QString::number(valor);
    }
    else
        qDebug() << "addNumero - Todas las cifras añadidas";
}

void Receiver::addLetra(QString valor)
{
    letras_.append(valor);
    //qDebug() << "LETRAS AHORA VALE: " << letras_;
}

void Receiver::resetCifras()
{
    numeros_.clear();
    listaNumeros_.clear();
    exacto_ = -1;
    //qDebug() << "Receiver::resetCifras();";
}

void Receiver::resetLetras()
{
    letras_ = "";
    //qDebug() << "Receiver::resetLetras();";
}

void Receiver::setValorCerebro(QString valor)
{
    valorLogradoCerebro_ = valor;
}

void Receiver::setNombreOro(QString valor)
{
    nombreOro_ = valor;
}

void Receiver::setpuntosOro(QString valor)
{
    puntosOro_ = valor;
}

void Receiver::setNombrePlata(QString valor)
{
    nombrePlata_ = valor;
}

void Receiver::setpuntosPlata(QString valor)
{
    puntosPlata_ = valor;
}

void Receiver::setNombreBronce(QString valor)
{
    nombreBronce_ = valor;
}

void Receiver::setpuntosBronce(QString valor)
{
    puntosBronce_ = valor;
}

QString Receiver::valorCerebro()
{
    return valorLogradoCerebro_;
}

QStringList Receiver::listaOperaciones()
{
    return listaOperaciones_;
}

QStringList Receiver::listaNumeros()
{
    return listaNumeros_;
}

QString Receiver::nombreOro()
{
    return nombreOro_;
}

QString Receiver::puntosOro()
{
    return puntosOro_;
}

QString Receiver::nombrePlata()
{
    return nombrePlata_;
}

QString Receiver::puntosPlata()
{
    return puntosPlata_;
}

QString Receiver::nombreBronce()
{
    return nombreBronce_;
}

QString Receiver::puntosBronce()
{
    return puntosBronce_;
}

QString Receiver::cargarRecords()
{
    QString ruta = getDataPath().append("/").append(ARCHIVO_RECORDS);
    qDebug() << "Intentamos cargar el archivo: " << ruta;

    QString salida="";
    if (QFile::exists(ruta))
    {
        QFile file(ruta);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qDebug() << "Error al abrir el archivo " << ruta << " para leer";
            salida = "-1";
        }
        else
        {
            QTextStream in(&file);
            salida = in.readAll().trimmed();
            file.close();
            qDebug() << "Archivo cargado correctamente con lo siguiente: " << salida;
        }
    }
    else
    {
        QFile file(ruta);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            qDebug() << "Error al abrir el archivo " << ruta << " para escribir";
            salida = "-2";
        }
        else
        {
            qDebug() << "El fichero no existia pero se ha creado correctamente";
            QTextStream out(&file);
            out << QString(ARCHIVO_VACIO);
            file.close();
            salida = QString(ARCHIVO_VACIO);
        }
    }
    return salida;
}


bool Receiver::guardarRecords()
{
    bool salida = false;
    QString ruta = getDataPath().append("/").append(ARCHIVO_RECORDS);
    qDebug() << "Intentamos guardar el archivo: " << ruta;
    if (QFile::exists(ruta))
    {
        QFile file(ruta);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            qDebug() << "Error al abrir el archivo " << ruta << " para escribir";
        else
        {
            QString textoArchivo = "";
            textoArchivo.append(nombreOro_).append(";").append(nombrePlata_).append(";");
            textoArchivo.append(nombreBronce_).append(";\n").append(puntosOro_).append(";");
            textoArchivo.append(puntosPlata_).append(";").append(puntosBronce_).append(";");

            QTextStream out(&file);
            out << textoArchivo;
            file.close();
            salida = true;
            qDebug() << "Records guardados en archivo " << ruta << " correctamente";
        }
    }

    return salida;
}

bool Receiver::checkDirs()
{
    QString mDataPath = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
    qDebug() << "Data Path: " << mDataPath;
    QDir myDir(mDataPath);
    if (!myDir.exists()) {
        bool ok = myDir.mkpath(mDataPath);
        if(!ok) {
            qWarning() << "Couldn't create dir. " << mDataPath;
            return false;
        }
        qDebug() << "created directory path" << mDataPath;
    }
    return true;
}

QString Receiver::getDataPath()
{
    if (!checkDirs())
        return "error";
    else
    {
        return QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
    }
}

bool Receiver::setRecords()
{
    bool salida = false;
    QString records = cargarRecords();
    if ((records == "-1") || (records == "-2"))
    {
        qDebug() << "Receiver::setRecords() -> Error cargando fichero de records";
    }
    else
    {
        QStringList lista = records.split("\n");
        QString nombres = lista.at(0);
        QString puntos = lista.at(1);
        QStringList listaNombres = nombres.split(";");
        QStringList listaPuntos = puntos.split(";");
        for (int i=0;i<listaPuntos.length();i++)
        {
            switch (i) {
            case 0:
                qDebug() << "Nombre Oro: " << listaNombres.at(i) << " - Puntos: " << listaPuntos.at(i);
                setNombreOro(listaNombres.at(i));
                setpuntosOro(listaPuntos.at(i));
                break;
            case 1:
                qDebug() << "Nombre Plata: " << listaNombres.at(i) << " - Puntos: " << listaPuntos.at(i);
                setNombrePlata(listaNombres.at(i));
                setpuntosPlata(listaPuntos.at(i));
                break;
            case 2:
                qDebug() << "Nombre Bronce: " << listaNombres.at(i) << " - Puntos: " << listaPuntos.at(i);
                setNombreBronce(listaNombres.at(i));
                setpuntosBronce(listaPuntos.at(i));
                break;
            default:
                qDebug() << "Nombre (" << i << "): " << listaNombres.at(i) << " - Puntos: " << listaPuntos.at(i);
                break;
            }
        }
        salida = true;
        qDebug() << "Receiver::setRecords() -> Records cargados (" << listaPuntos.length() << ")";
    }
    return salida;
}

bool Receiver::reiniciarRecords()
{
    bool salida = false;
    QString ruta = getDataPath().append("/").append(ARCHIVO_RECORDS);
    qDebug() << "Intentamos reiniciar el archivo: " << ruta;

    if (QFile::exists(ruta))
        QFile::remove(ruta);

    QFile file(ruta);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Error al abrir el archivo " << ruta << " para escribir";
    }
    else
    {
        qDebug() << "El fichero se ha reiniciado correctamente";
        QTextStream out(&file);
        out << QString(ARCHIVO_VACIO);
        file.close();
        salida = true;
    }

    return salida;
}

void Receiver::salirDelJuego()
{
    QGuiApplication::exit();
}

bool Receiver::abrirNavegador(QString url)
{
    return QDesktopServices::openUrl(QUrl(url, QUrl::TolerantMode));
}

bool Receiver::enviarMail()
{
    return QDesktopServices::openUrl(QUrl("mailto:appsbydresoft@gmail.com?subject=Dudas o sugerencias&body=", QUrl::TolerantMode));
}

QString Receiver::eliminarCaracteresPeligrosos(QString entrada)
{
    QString salida = entrada;
    salida = salida.replace("\\n","");
    salida = salida.replace(";","");
    salida = salida.replace("<br>","");
    return salida;
}
