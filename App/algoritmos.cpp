#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <QList>
#include <QDebug>

#include "algoritmos.h"


Algoritmos::Algoritmos(QObject *parent)
{
    Q_UNUSED(parent);
    //qDebug() << "CREAMOS EL OBJETO ALGORITMOS";
}

//Metodo de busqueda total, devuelve el valor mas aproximado al exacto encontrado (entero)
//y el camino para lograrlo (QStringlist con las operaciones realizadas)
QPair <int,QStringList> Algoritmos::ejecutarBusqueda(QList<int> lista,int exacto)
{
    QPair<int,QStringList> salida;

    if (lista.length() != 6)
    {
        qDebug() << "ERROR!! No hay 6 elementos en la lista ";
        salida.first = -1;
        salida.second = QStringList();
        return salida;
    }

// NO MOSTRAMOS LOGS - Si se necesita se puede quitar

//    qDebug() << "============================================================================================";
//    qDebug() << "==============================<< OPERACIONES CEREBRO >>=====================================";
//    qDebug() << "============================================================================================";
//    qDebug() << "= CIFRAS =>" << lista.at(0) << "," << lista.at(1) << "," << lista.at(2) << ","
//             << lista.at(3) << "," << lista.at(4) << "," << lista.at(5);
//    qDebug() << "= EXACTO =>" << exacto;

    contador i;
    int Lista[N];
    for (i=0;i<N;i++)
    {
        Lista[i]=lista.at(i);
    }
    QString Camino[N];
    bool exito;

    exacto_ = exacto;
    ordenarNodo (Lista);
    for ( i=0 ; i<N ; i++ )
        Camino[i] = QString::number(Lista[i]);

    cambiaMejor (Lista[0],Camino[0]);
    numTotalNodos_ = -1;
    numNodosVistos_ = -1;

    if ( diferencia_ )
        exito = busca (Lista,Camino);
    else
        exito = SI;

    Q_UNUSED(exito);

    QString resultado;
    (mejor_ == exacto) ?  resultado = "EXACTO ENCONTRADO!!": resultado = "EXACTO NO ENCONTRADO";

    //Devolvemos un par con el valor mas cercano al exacto y el camino para conseguirlo
    salida.first = mejor_;
    salida.second = muestraOperaciones(mejorCamino_);

    // NO MOSTRAMOS LOGS - Si se necesita se puede quitar

    //    qDebug() << "= NUMERO CONSEGUIDO POR CEREBRO =>" << mejor_;
//    qDebug() << "= CAMINO =>" << muestraOperaciones(mejorCamino_);
//    qDebug() << "= RESULTADO => " << resultado;
    return salida;
}

bool Algoritmos::busca(int L[N],QString C[N])
{
    int   L2[N];
    int   Lo[N];
    QString   C2[N];
    bool   exito;
    bool   comprobado;
    contador Elemento, Operacion, i, j, k;
    int   A, B;
    QString   CaminoA, CaminoB;

    exito = NO;
    Elemento = 0;

    for ( i=0 ; i<N && !exito ; i++ )
        {
        if ( L[i] )
          {
          if ( distancia ( L[i],exacto_ ) < diferencia_ )
            {
            cambiaMejor ( L[i], C[i] );
            if ( L[i]==exacto_ )
              { exito = SI; }
            }
          Elemento++;
          }
        }

      comprobado = NO;
      if ( Elemento == N-2 )
        {
        for ( k=0 ; k<N ; k++ )
          { Lo[k] = L[k]; }
        ordenarNodo(Lo);
        comprobado = perteneceANodo(Lo);
        }

      if ( !exito && Elemento == 2 )
        { exito = buscaEnDos (L,C); }

      if ( !exito && Elemento>2 && !comprobado )
        {
        for ( i=0 ; i<N-1 && !exito ; i++ )
          {
          if ( L[i] )
            {
            for ( j=i+1 ; j<N && !exito ; j++ )
              {
              if ( L[j] )
                {
                for ( k=0 ; k<N ; k++ )
                  {
                  L2[k] = L[k];
                  copiarCadena(C2[k],C[k]);
                  }

                A = L2[i]; copiarCadena(CaminoA,C2[i]);
                B = L2[j]; copiarCadena(CaminoB,C2[j]);
                L2[j] = 0; C2[j][0] = NULO;
                for ( Operacion=SUMA ; Operacion<=DIVISION && !exito ;
                                       Operacion++ )
                  {
                  switch ( Operacion )
                    {
                    case SUMA:
                      L2[i] = A+B;
                      concatenarCamino ( C2[i], CaminoA, "+", CaminoB );
                      exito = busca (L2,C2);
                      break;
                    case RESTA:
                      if ( A>B )
                        {
                        L2[i] = A-B;
                        concatenarCamino ( C2[i], CaminoA, "-", CaminoB );
                        exito = busca (L2,C2);
                        }
                      else if ( B>A )
                        {
                        L2[i] = B-A;
                        concatenarCamino ( C2[i], CaminoB, "-", CaminoA );
                        exito = busca (L2,C2);
                        }
                      break;
                    case MULTIPLICACION:
                      L2[i] = A*B;
                      concatenarCamino ( C2[i], CaminoA, "*", CaminoB );
                      exito = busca (L2,C2);
                      break;
                    case DIVISION:
                      if ( (A % B) == 0 )
                        {
                        L2[i] = A/B;
                        concatenarCamino ( C2[i], CaminoA, "/", CaminoB );
                        exito = busca (L2,C2);
                        }
                      else if ( (B % A) == 0 )
                        {
                        L2[i] = B/A;
                        concatenarCamino ( C2[i], CaminoB, "/", CaminoA );
                        exito = busca (L2,C2);
                        }
                      break;
                    }
                  }
                }
              }
            }
          }
        }

      if ( Elemento == N-2 )
        {
        if ( !comprobado )
          { insertarNodo(Lo); }
        numTotalNodos_++;
        }

      return ( exito );
}

bool Algoritmos::buscaEnDos(int L[N],QString C[N])
{
    bool exito = NO;
    bool TengoElPrimero = NO;
    contador Primero, Segundo;
    int Num;
    QString Camino;
    contador i, Operacion;

    Primero = 0; Segundo = 0;
    for ( i=0 ; !Segundo ; i++ )
      {
      if ( L[i] )
        {
        if ( !TengoElPrimero )
          {
          Primero = i;
          TengoElPrimero = SI;
          }
        else
          { Segundo = i; }
        } /* Fin del if   */
      } /* Fin del for i */

    for ( Operacion=SUMA ; Operacion<=DIVISION && !exito ;
                           Operacion++ )
      {
      switch ( Operacion )
        {
        case SUMA:
          Num = L[Primero]+L[Segundo];
          if ( distancia ( Num,exacto_ ) < diferencia_ )
            {
            concatenarCamino ( Camino, C[Primero], "+", C[Segundo] );
            cambiaMejor ( Num, Camino );
            if ( Num==exacto_ )
              { exito = SI; }
            }
          break;
        case RESTA:
          if ( L[Primero]>L[Segundo] )
            {
            Num = L[Primero]-L[Segundo];
            if ( distancia ( Num,exacto_ ) < diferencia_ )
              {
              concatenarCamino ( Camino, C[Primero], "-", C[Segundo] );
              cambiaMejor ( Num, Camino );
              if ( Num==exacto_ )
                { exito = SI; }
              }
            }
          else if ( L[Segundo]>L[Primero] )
            {
            Num = L[Segundo]-L[Primero];
            if ( distancia ( Num,exacto_ ) < diferencia_ )
              {
              concatenarCamino ( Camino, C[Segundo], "-", C[Primero] );
              cambiaMejor ( Num, Camino );
              if ( Num==exacto_ )
                { exito = SI; }
              }
            }
          break;
        case MULTIPLICACION:
          Num = L[Primero]*L[Segundo];
          if ( distancia ( Num,exacto_ ) < diferencia_ )
            {
            concatenarCamino ( Camino, C[Primero], "*", C[Segundo] );
            cambiaMejor ( Num, Camino );
            if ( Num==exacto_ )
              { exito = SI; }
            }
          break;
        case DIVISION:
          if ( (L[Primero] % L[Segundo]) == 0 )
            {
            Num = L[Primero]/L[Segundo];
            if ( distancia ( Num,exacto_ ) < diferencia_ )
              {
              concatenarCamino ( Camino, C[Primero], "/", C[Segundo] );
              cambiaMejor ( Num, Camino );
              if ( Num==exacto_ )
                { exito = SI; }
              }
            }
          else if ( (L[Segundo] % L[Primero]) == 0 )
            {
            Num = L[Segundo]/L[Primero];
            if ( distancia ( Num,exacto_ ) < diferencia_ )
              {
              concatenarCamino ( Camino, C[Segundo], "/", C[Primero] );
              cambiaMejor ( Num, Camino );
              if ( Num==exacto_ )
                { exito = SI; }
              }
            }
          break;
        } /* Fin del switch        */
      } /* Fin del for Operacion */

    return (exito);
}


void Algoritmos::cambiaMejor(int Nuevo,QString NuevoCamino)
{
    mejor_ = Nuevo;
    mejorCamino_ = NuevoCamino;
    diferencia_ = distancia ( mejor_,exacto_ );
}

QString Algoritmos::concatenarCamino(QString &destino,QString Primero,QString Operacion,QString Segundo)
{
    QString aux(SEPARADOR);
    aux.append(Segundo);
    aux.append(SEPARADOR);
    aux.append(Operacion);
    destino = Primero;
    destino.append(aux);

    return destino;
}

Algoritmos::nodo Algoritmos::ordenarNodo(int L[N])
{
    contador i, j;
    int   num;

    for ( i=1 ; i<N ; i++ )
      {
      num = L[i];
      for ( j=i-1 ; j>=0 && L[j]<num ; j-- )
        { L[j+1] = L[j]; }
      L[j+1] = num;
      }

    return (L);
}

bool Algoritmos::perteneceANodo(nodo L)
{
    bool Encontrado = NO;
    bool VaSiendoIgual;
    contador i, j;

    for ( i=0 ; i<=numNodosVistos_ && !Encontrado ; i++ )
      {
      VaSiendoIgual = SI;
      for ( j=0 ; j<N-2 && VaSiendoIgual ; j++ )
        {
          if ( L[j] != comprobado[i][j] )
            { VaSiendoIgual = NO; }
        }
      Encontrado = VaSiendoIgual;
      }
    return ( Encontrado );
}

void Algoritmos::insertarNodo(nodo L)
{
    contador i;

    numNodosVistos_++;
    for ( i=0 ; i<N-2 ; i++ )
      { comprobado[numNodosVistos_][i] = L[i]; }
}

int Algoritmos::calcular(int val1,int val2, QString op)
{
    int res = 0;
    if (op == "+")
        res = val1 + val2;
    else if (op == "-")
        res = val1 - val2;
    else if (op == "*")
        res = val1 * val2;
    else if (op == "/")
        res = val1 / val2;

    return res;
}

QStringList Algoritmos::muestraOperaciones(QString expresion)
{
    QStringList elementos = expresion.split(',');
    QStringList listaOperaciones;

    QString auxOp = "";

    int cont = elementos.length() - 1;
    int a1,a2,aOp,res,val1,val2 = 0;
    QString valOp;

    //Si un numero es directamente el exacto lo a�adimos al camino
    if (elementos.length() == 1)
        listaOperaciones << elementos[0];

    //Primero hacemos las operaciones sueltas, quitamos todas las que no sigan el hilo principal de operaciones
    while (cont>1)
    {
        if (!esOperacion(elementos.at(cont)) && (!esOperacion(elementos.at(cont-1))))
        {
            auxOp = "";
            a2 = cont;
            a1 = cont -1;
            aOp = cont + 1;
            val1 = elementos.at(a1).toInt();
            val2 = elementos.at(a2).toInt();
            valOp = elementos.at(aOp);

            //Hacemos el calculo
            res = calcular(val1,val2,valOp);

            //Lo guardamos en la lista de operaciones
            auxOp.append(elementos.at(a1));
            auxOp.append(" ");
            auxOp.append(elementos.at(aOp));
            auxOp.append(" ");
            auxOp.append(elementos.at(a2));
            auxOp.append(" = ");
            auxOp.append(QString::number(res));
            listaOperaciones << auxOp;

            //sustituimos los 3 valores (val1,val2 y la operacion, por el resultado final)
            elementos[a1]  = QString::number(res);
            elementos.removeAt(aOp);
            elementos.removeAt(a2);

            //Ponemos el contador al inicio para repetir hasta que no haya numeros seguidos salvo en las 2 primeras posiciones
            cont = elementos.length() - 1;;
        }
        else
          cont--;
    };

    //Aqui ya tenemos todos individuales excepto los 2 primeros elementos asi q comenzamos a operar en un orden fijo
    while (elementos.length() > 1)
    {
        auxOp = "";
        a1 = 0; a2 = 1; aOp = 2;
        val1 = elementos.at(a1).toInt();
        val2 = elementos.at(a2).toInt();
        valOp = elementos.at(aOp);
        res = calcular(val1,val2,valOp);

        //Lo guardamos en la lista de operaciones
        auxOp.append(elementos.at(a1));
        auxOp.append(" ");
        auxOp.append(elementos.at(aOp));
        auxOp.append(" ");
        auxOp.append(elementos.at(a2));
        auxOp.append(" = ");
        auxOp.append(QString::number(res));
        listaOperaciones << auxOp;

        //sustituimos los 3 valores (val1,val2 y la operacion, por el resultado final)
        elementos[a1]  = QString::number(res);
        elementos.removeAt(aOp);
        elementos.removeAt(a2);
    }

    //Mostramos las operaciones por pantalla (Comentado)
    //    for (int j=0;j<listaOperaciones.length();j++)
    //    {
    //        qDebug() << "OPERACION " << j+1 << ": " << listaOperaciones.at(j);
    //    }

    //Devolvemos una lista con todas las operaciones realizadas
    return listaOperaciones;
}


QString Algoritmos::cadTrozo(QString destino,QString origen,contador comienzo,contador fin)
{
    contador i;

    for ( i=comienzo ; i<=fin ; i++ )
      {
      destino[i-comienzo] = origen[i];
      }

    destino[i-comienzo] = NULO;

    return (destino);
}

void Algoritmos::iniciarCalculadora()
{
    contador i;

      for ( i=0 ; i<N ; i++ )
        { pilaNumeros[i] = 0; }

}

int Algoritmos::operar(QChar Operacion)
{
    contador i;
      int Num = 0;

      if (Operacion == '+')
          Num = pilaNumeros[1]+pilaNumeros[0];
      else if (Operacion == '-')
          Num = pilaNumeros[1]-pilaNumeros[0];
      else if (Operacion == '*')
          Num = pilaNumeros[1]*pilaNumeros[0];
      else if (Operacion == '/')
          Num = pilaNumeros[1]/pilaNumeros[0];

     //qDebug() << pilaNumeros[1] << " - " << Operacion << " - " << pilaNumeros[0] << " - " << Num;

      pilaNumeros[0] = Num;

      for ( i=1 ; i<N-1 ; i++ )
        { pilaNumeros[i] = pilaNumeros[i+1]; }

      return (Num);
}

int Algoritmos::insertar(int Num)
{
    contador i;

    for ( i=N-1 ; i>0 ; i-- )
      { pilaNumeros[i] = pilaNumeros[i-1]; }

    pilaNumeros[0] = Num;

    return (Num);
}
