.pragma library


var textoAyudaRondaRapidaCifras = "<b>Objective:</b> <br><br> The goal is to get a randomly chosen number between 101 and 999 \
with the elementary arithmetic operations (+, -, ×, ÷) with six numbers (from 1 to 10, 25, 50, 75 and 100). You do not have to \
use all numbers, but you can not repeat any. The time to find the exact one is 60 seconds, although <b> we can \
surrender before, pressing on the circle of countdown timer once a white flag has appeared, pressing again</b>. If it isn't pressed \
for a second time, it will quickly display the remaining seconds. <br><br>\
<b>Game Mechanics</b>: <br><br> To perform the operations, we will choose a number by clicking on it (the number box will turn blue), \
Then select the operation that we want to perform and then the next operand. If the operation can be \
performed, a new number containing the result of the operation will be created. For example, we select a 6, \
the operation multiply (X) and then the last operand, in this case 3. The result will be a new frame with the result of \
the operation, ie 18. <br> In the lower box will be shown the number closest to the exact one that we have found automatically \
without the need to press any button or do anything. If we find the exact one, the game ends. <br><br><b>Special cases for subtractions</b>:<br><br> - If we try to subtract to a \
number other major, the result will be the subtraction of the major minus the minor, \
therefore, all subtraction operations can be performed. <br> - If we subtract to a number another number of the same value, the result will be zero and the number \
will disappear.<br><br><b>Special cases for divisions</b>:<br><br> - If we try to divide a number between another greater, and \
the first one is divisor of the second, it will happen as with the subtraction and the greatest of the two numbers will be divided by the minor. <br> - If we try to divide a number by another \
that is not divisor or multiple of it, the numbers will turn red one second to show the error and the selection of both will be removed. <br><br> \
<b> Buttons </b>:<br><br>'Undo': Clicking this button will undo the last movement<br> 'Begin': all movements done will be eliminated \
and we will start the operations again"

var tituloAyudaRondaRapidaCifras = "Training numbers round"
var textoAyudaRondaRapidaLetras = "<b>Objective</ b>:<br><br> The goal is to find the longest possible word without using any letter more than once. \
The words included in the Oxford dictionary are valid. Proper nouns are not allowed. The frequencies of the letters within each stack are weighted according \
to their frequency in natural English, in the same manner as Scrabble. The time to find the word is 60 seconds, although <b> we can \
surrender before, pressing on the circle of countdown timer once a white flag has appeared, pressing again</b>. If it isn't pressed \
for a second time, it will quickly display the remaining seconds. <br><br>\
<b>Game Mechanics</b>:<br><br> By clicking on the letters obtained randomly, a new word will be formed. If the word exists, the letters of the new \
word formed will be green, if it does not exist, it will be red. As we are discovering words of greater length, the longest word found will be stored at the bottom of the screen, \
and to its right there will be a circle with the number of letters of the same.<br>To include or remove letters to the word formed, we can either click on the letter in \
the top box, or we can act with the buttons that will appear when there is at least one letter of the top selected. <br><br> \
<b> Buttons </b>:<br><br> 'Undo': Clicking this button will undo the last movement<br> 'Begin': Eall movements done will be eliminated\
And we start again to search for a word.<br> 'Mix': Move the letters to change the point of view."

var tituloAyudaRondaRapidaLetras = "Training letters round"
var textoAyudaPartidaCompleta = " The countdown game is a series of <b>10 rounds</b> divided between numbers and letters as follows: <br><br> - Numbers, letters, letters,\
letters, letters, numbers, letters, letters and numbers. <br><br><b>  It is about getting the highest number of points by adding all the rounds </ b>. The time for each round is 45 seconds, although <b> we can \
surrender before, pressing on the circle of countdown timer once a white flag has appeared, pressing again</b>. If it isn't pressed \
for a second time, it will quickly display the remaining seconds. <br><br>\
 At the bottom of the screen, there is a marker that shows if the round has been of numbers or letters and the points achieved in each one of them. The points are distributed as follows: <br><br><b> \
Points distribution </ b>: <br> <br> <b> Letters </ b>: <br> - One point for each letter of the word found. <br><br><b>Numbers</b>:<br> - 8 points for the exact one <br> - 6 points if you stay as maximum as 2 of difference.<br> - 5 points if you stay at \
Between 3 and 5 points <br> - 4 points if you stay a maximum of 10 difference <br> - 3 points up to 20. <br> - 2 points up to 50. <br> - 1 point if you stay as Maximum to 100 of difference. <br> - 0 points if you stay more than \
100 of difference <br> <br> <b>Finishing of the game </ b>: <br><br>  Once the game is over, if the result enters the ranking of records (among the top 3) <b> we must enter the name \
of the player with a maximum of eight characters since otherwise the result will not be saved. </b>It only scores in the ranking of records with the countdown game."

var tituloAyudaPartidaCompleta = "Countdown game"
var textoAyudaRecords = "This screen shows the ranking with the three best results achieved in a countdown game. The result must have been previously saved \
from the finished game notice in countdown game. <br><br><b> The maximum score that can be obtained is 86 </b> (32 points of numbers and 54 of letters)."
var tituloAyudaRecords = "Records"

function getTextoAyuda(pantalla)
{
    var salida = ""
    switch (pantalla)
    {
        case 1: salida = textoAyudaRondaRapidaCifras
            break
        case 2: salida = textoAyudaRondaRapidaLetras
            break
        case 3: salida = textoAyudaPartidaCompleta
            break
        case 4: salida = textoAyudaRecords
            break
        default:
            break
    }
    console.log("Ayuda.getTextoAyuda("+pantalla+")")
    return salida
}

function getTituloAyuda(pantalla)
{
    var salida = ""
    switch (pantalla)
    {
        case 1: salida = tituloAyudaRondaRapidaCifras
            break
        case 2: salida = tituloAyudaRondaRapidaLetras
            break
        case 3: salida = tituloAyudaPartidaCompleta
            break
        case 4: salida = tituloAyudaRecords
            break
        default:
            break
    }
    console.log("Ayuda.getTituloAyuda("+pantalla+")")
    return salida
}
