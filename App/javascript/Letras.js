//LETRAS INICIALES Y OPERACIONES CON LAS LETRAS
console.log("INICIO creación Letras.js")

//English-language editions of Scrabble contain 100 letter tiles, in the following distribution:

//E ×12, A ×9, I ×9, O ×8, N ×6, R ×6, T ×6, L ×4, S ×4, U ×4, D ×4, G ×3
//B ×2, C ×2, M ×2, P ×2, F ×2, H ×2, V ×2, W ×2, Y ×2, K ×1, J ×1, X ×1, Q ×1, Z ×1

//Consontantes iniciales
var consonantesIniciales = ['B','B','C','C','D','D','D','D','F','F','G','G','G','H']
consonantesIniciales.push('H','J','K','L','L','L','L','M','M','N','N','N','N','N','N')
consonantesIniciales.push('P','P','Q','R','R','R','R','R','R','S','S','S','S','T','T')
consonantesIniciales.push('T','T','T','T','V','V','W','W','X','Y','Y','Z')

//Vocales Iniciales
var vocalesIniciales = ['A','A','A','A','A','A','A','A','A','E','E','E','E','E','E']
vocalesIniciales.push('E','E','E','E','E','E','I','I','I','I','I','I','I','I','I')
vocalesIniciales.push('O','O','O','O','O','O','O','O','U','U','U','U')

var consonantesRestantes = []
var vocalesRestantes = []

var movimientos = []
var letrasPartida = []

var numVocales = 0
var numConsonantes = 0

generaPartida()
console.log("FIN creación Letras.js")

//Obtener letras
function getConsonante ()
{
    var indice = Math.floor(Math.random() * (consonantesRestantes.length - 1))
    var cons = consonantesRestantes[indice]
    consonantesRestantes.splice(indice,1)
    return cons
}

function getVocal()
{
    var indice = Math.floor(Math.random() * (vocalesRestantes.length - 1))
    var voc = vocalesRestantes[indice]
    vocalesRestantes.splice(indice,1)
    return voc
}

function getLetra()
{
    //Si las consonantes restantes o las vocales restantes estan vacias es que es el primer inicio y hay
    //que inicializar los arrays
        //console.log("Consonantes ==> Iniciales: " + consonantesIniciales.length + " - restantes: "+consonantesRestantes.length)
        //console.log("Vocales ==> Iniciales: " + vocalesIniciales.length + " - restantes: "+vocalesRestantes.length)

    if ((vocalesRestantes.length === 0) || (consonantesRestantes.length === 0))
        iniciarArrays();

    //50% de posibilidades de obtener una vocal o una consonante
    var salida = '';
    var random = Math.random()*2
    var floor = Math.floor(random)
        //console.log("RANDOM = " + random + " FLOOR " + floor)

//    console.log("numConsonantes = "+numConsonantes)
//    console.log("numVocales = "+numVocales)
    //Limitamos las posibles vocales o consonantes
    //por cada dos consonantes o vocales sacadas, sacamos la contraria
    if (numConsonantes - numVocales > 2)
    {
        //console.log("hay "+(numConsonantes - numVocales)+" consonantes mas que vocales, sacamos vocal")
        floor = 0
    }
    else if (numVocales - numConsonantes > 2)
    {
        //console.log("hay "+(numVocales - numConsonantes)+" vocales mas que consonantes, sacamos consonante")
        floor = 1
    }

    switch(floor)
    {
    case 0: salida = getVocal()
        numVocales++
        break
    default: salida = getConsonante()
        numConsonantes++
        break
    }

    return salida
}

//Iniciar / Inicializar arrays - Reiniciar partida
function iniciarArrays()
{
    console.log("Letras::iniciarArrays()")
    numConsonantes = 0
    numVocales = 0
    vocalesRestantes = []
    consonantesRestantes = []
    letrasPartida = []
    movimientos = []
    for (var i=0; i<vocalesIniciales.length; i++)
        vocalesRestantes.push(vocalesIniciales[i])
    for (var j=0; j<consonantesIniciales.length; j++)
        consonantesRestantes.push(consonantesIniciales[j])
}

// -- Añadir o eliminar movimientos ---------------------
function pushMovimiento(indiceEnLetras)
{
    //console.log("Añadir MOVIMIENTO ficha "+ indiceEnLetras)
    movimientos.push(indiceEnLetras)
}

function popMovimiento()
{
    return movimientos.pop()
}

function pushLetrasPartida(letra)
{
    letrasPartida.push(letra)
    console.log("AÑADIMOS la letra: "+letra)
    if (letrasPartida.length == 9)
        console.log("== LETRAS PARTIDA: "+letrasPartida+" ==")
}

function resetMovimientos()
{
    console.log("Letras::resetMovimientos()")
    movimientos = []
}

function reiniciar()
{
    console.log("Letras::reiniciar()")
    iniciarArrays()
}

function generaPartida()
{
    reiniciar()
    var salida = ""
    for (var i=0;i<9;i++)
        letrasPartida.push(getLetra())

    console.log("Partida generada con las letras: "+letrasPartida)
    console.log("Letraspartida.lenght = "+letrasPartida.length)
    return letrasPartida
}

function setPartida(datosPartida)
{
    reiniciar()
    console.log("DatosPartida: "+datosPartida)
    for (var i=0;i<9;i++)
//        pushLetrasPartida(datosPartida.substring(i,i+1))
        letrasPartida.push(datosPartida.substring(i,i+1))
    console.log("PARTIDA establecida con las letras: "+letrasPartida)
    console.log("letrasPartida.length= "+letrasPartida.length)
}

function getLetraPartida()
{
    var salida = ''
    console.log("getLetraPartida length:"+letrasPartida.length)
    if(letrasPartida.length > 0)
        salida = letrasPartida.pop()

    console.log("getLetraPartida = "+salida)
    return salida
}
