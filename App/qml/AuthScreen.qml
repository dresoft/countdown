import QtQuick 2.0

Item {

    id: auth
    anchors.fill: parent

    property int radio: 6
    Rectangle {
        anchors.fill: parent
        id: fondo
        color: "slategray"
    }

    //Rectangulo central
    Item {

        id: central
        width: parent.width * 0.7
        height: parent.height* 0.3
        anchors.centerIn: parent

        //boton google
        Rectangle {

            id: googleButton
            height: central.height*0.5
            width: parent.width
            anchors.horizontalCenter:  parent.horizontalCenter
            anchors.top: parent.top
            color:  "salmon"
            radius: radio

            Image {
                height: parent.height*0.7
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: width*0.1
                anchors.left: parent.left
                source: "qrc:/images/images/googleIcon.png"

                Text {
                    id: txtGoogle
                    color: "#ffffff"
                    text: qsTr("Google")
                    font.family: "American Typewriter"
                    font.bold: true
                    width: googleButton.width - parent.width
                    height: parent.height
                    anchors.left: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: height * 0.4
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: //Autenticacion en google
                           console.log("Nos autenticamos en Google")
            }

        }

        //boton facebook
        Rectangle {

            id: facebookButton
            height: central.height*0.5
            width: parent.width
            anchors.horizontalCenter:  parent.horizontalCenter
            anchors.bottom: parent.bottom
            color:  "lightblue"
            radius: radio

            Image {
                height: parent.height*0.7
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: width*0.1
                anchors.left: parent.left
                source: "qrc:/images/images/facebookIcon.png"

                Text {
                    id: txtfacebok
                    color: "#ffffff"
                    text: qsTr("Facebook")
                    font.family: "Chalkboard SE"
                    font.bold: true
                    width: facebookButton.width - parent.width
                    height: parent.height
                    anchors.left: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: height * 0.4
                }
            }


            MouseArea {
                anchors.fill: parent
                onClicked: //Autenticacion en facebook
                           console.log("Nos autenticamos en Facebook")
            }

        }

        Text {
            id: txtLogin
            text: "Inicia sesión con: "
            font.family: "Chalkboard SE"
            height: auth.height*0.1
            font.pixelSize: height*0.5
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.top
            anchors.bottomMargin: height*0.05
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    Image {

        id: log
        source: "qrc:/images/images/user.png"
        height: auth.height*0.18
        width: height
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: auth.top
        anchors.topMargin: height*0.2
    }


}
