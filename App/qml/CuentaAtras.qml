import QtQuick 2.3
import QtQuick.Controls 1.2


Item {

    id: root

    property bool running: false
    property int valorInicialSegundos
    property int valorSegundos: valorInicialSegundos
    property bool activo: false //Se desactiva solo al llegar a 0 segundos
    signal tiempoAgotado
    signal quedan10segs

    Rectangle {
        id: fondo
        width: root.width
        height: width
        anchors.centerIn: texto
        color: "green"
        radius: 400
        border.width: 3
        border.color: "black"
    }

    Text
    {
        id: texto
        anchors.centerIn: fondo
        text: valorSegundos
        font.family: "Sawasdee"
        font.bold: true
        style: Text.Outline
        font.pixelSize: root.height/1.8
        color: "white"
        //cambiar a fuente parecida a reloj digital
    }

    BotonImagen {

        id: rendirse
        visible: false
        anchors.fill: fondo
        anchors.centerIn: fondo
        z: 1
        imagePath: "qrc:/images/images/banderaBlanca.png"
        noCambiarOpacidad: true

        Timer {
            id: timerBandera
            running: false
            interval: 1500
            repeat: false
            onTriggered: {
                rendirse.visible = false
            }
        }

        onPulsado: {
            rendirse.visible = false
            tiempoAgotado()
            arrancarPararCrono()
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        enabled: activo
        onClicked: {
            if ((valorSegundos > 2) && (valorSegundos < valorInicialSegundos) &&(tempSegundos.running === true))
            {
                rendirse.visible = true
                timerBandera.running = true
            }
        }
    }

    Timer {
        id: tempSegundos
        interval: 1000; running: false; repeat: true
        onTriggered: {

            //restamos 1 segundo
            if (valorSegundos>0)
                valorSegundos--

            //Cambiamos el color del borde
            if ((valorSegundos % 2 === 0) && (valorSegundos>0))
                fondo.border.color = "white"
            else
                fondo.border.color = "black"

            //Cambiamos el texto de color segun el tiempo que quede
            var porcentaje = getPorcentajeTiempo()
            switch (true)
            {
                case (porcentaje > 75):
                   fondo.color = "green"
                   break
                case (porcentaje > 50):
                    fondo.color = "gold"
                    break
                case (porcentaje > 25):
                    fondo.color = "orange"
                    break
                case (porcentaje > 10):
                    fondo.color = "orangered"
                    break
                default:
                    fondo.color = "red"
                    break
            }

            //paramos el crono al llegar a cero
            if (valorSegundos == 0)
            {
                arrancarPararCrono()
                fondo.color = "red"
                texto.color = "white"
                fondo.border.color = "black"
                tiempoAgotado()
                activo = false
            }

            if (valorSegundos == 10)
                quedan10segs()
        }
    }

    //SLOTS
    function arrancarPararCrono () {

        if (tempSegundos.running == true)
        {
            tempSegundos.running = false
            running = false
            fondo.border.color = "black"
        }
        else
        {
            if(activo){
                tempSegundos.running = true
                running = true
            }
        }
    }

    function reset()
    {
        texto.color = "white"
        fondo.color = "green"
        fondo.border.color = "black"
        tempSegundos.running = false
        valorSegundos = valorInicialSegundos
        activo = false
    }

    function restart()
    {
        texto.color = "white"
        fondo.color = "green"
        fondo.border.color = "black"
        tempSegundos.running = false
        valorSegundos = valorInicialSegundos
        activo = true
        arrancarPararCrono()
    }

    function getPorcentajeTiempo()
    {
        return Math.floor((valorSegundos/valorInicialSegundos)*100)
    }
}
