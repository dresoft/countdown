import QtQuick 2.3
import QtQuick.Controls 1.2


Item {

    id: root

    property string text: texto.text
    property bool activo: false
    property bool seleccionada: false
    property int indice //lugar que ocupa la cifra en el modelo de cifras actuales
    property string colorTexto
    property int tamanoFuenteDefecto: height/2
    property string colorFondo
    signal cifraPulsada (int index)

    Rectangle {

        id: fondo
        width: parent.width*0.90
        height: parent.height*0.90
        anchors.centerIn: parent
        radius: 4
        color: "white"
    }

    Text {
        id: texto
        anchors.fill: fondo
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: "#000000"
        text: root.text
    }

    onTextChanged:  {
        ajustarTexto();
    }

    onColorFondoChanged: {
        fondo.color = colorFondo;
    }

    onColorTextoChanged:{
        texto.color = colorTexto;
        if ((colorTexto != "black") || (colorTexto != "white"))
            volverATextoNegro.restart();
    }

    Timer {
        id: volverATextoNegro
        interval: (colorTexto == "green") ? 500 : 400
        running: false
        repeat: false
        onTriggered: (colorFondo == "blue") ? texto.color = "white" : texto.color = "black"
    }


    MouseArea {

        id: mouseArea
        anchors.fill: root
        onClicked:
        {
            if (activo)
            {
                if (!seleccionada)
                    seleccionada=true;
                else
                    seleccionada=false;

                cifraPulsada(indice);
            }
        }
    }

    onSeleccionadaChanged:
    {
        if (seleccionada)
        {
            colorFondo = "blue";
            colorTexto = "white";
        }
        else
        {
            colorFondo = "white";
            colorTexto = "black";
        }


    }

    Component.onCompleted: ajustarTexto()

// --- Funciones ------------------------------------------------------------------

    function ajustarTexto ()
    {
        if (text.length > 3)
            texto.font.pixelSize = tamanoFuenteDefecto * (3/text.length);
        else
            texto.font.pixelSize = tamanoFuenteDefecto
    }

    function seleccionar()
    {
        seleccionada = true;
    }

    function deseleccionar()
    {
        seleccionada = false;
    }
}
