import QtQuick 2.3
import QtQuick.Controls 1.2


Item {

    id: root

    //variables del modelo de datos
    property string text: texto.text
    property int indice //lugar que ocupa la letra en el modelo de letras
    property int indiceEnPalabra: -1 //lugar que ocupara la letra en la palabra generada

    property string colorTexto
    property string colorFondo
    property int tamanoFuenteDefecto: height/2

    property bool enPruebas: false //Lo activamos para ver los superindices en pruebas

    property bool activo
    property bool usada: false

    Rectangle {

        id: fondo
        width: parent.width*0.90
        height: parent.height*0.90
        anchors.centerIn: parent
        radius: 4
        color: "white"
    }

    Text {
        id: texto
        anchors.fill: fondo
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: "#000000"
        text: root.text
    }

    //Texto para probar, normalmente visible false
    Text {
        id: textoIndiceLetraPequeña
        anchors.horizontalCenter: fondo.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: "purple"
        text: indiceEnPalabra
        font.pixelSize: parent.height/5
        visible: enPruebas
    }

    MouseArea {
        id: mouseArea
        anchors.fill: root
        onClicked:
        {
            if (activo)
            {
                usada = !usada
            }
        }
    }

    onTextChanged: ajustarTexto()
    onColorFondoChanged: fondo.color = colorFondo
    onUsadaChanged:
    {
        if (usada)
        {
            fondo.opacity = 0.5
            texto.opacity = 0.5
        }
        else
        {
            fondo.opacity = 1
            texto.opacity = 1
        }
    }

    Component.onCompleted: ajustarTexto()

// --- Funciones ------------------------------------------------------------------

    function ajustarTexto ()
    {
        if (text.length > 3)
            texto.font.pixelSize = tamanoFuenteDefecto * (3/text.length)
        else
            texto.font.pixelSize = tamanoFuenteDefecto
    }

    function seleccionar()
    {
        seleccionada = true
    }

    function deseleccionar()
    {
        seleccionada = false
    }

    //Simulamos el pulsado
    function clicked()
    {
        if (activo)
            usada = !usada
    }

    function reset()
    {
        activo = false;
        usada = false;
    }
}
