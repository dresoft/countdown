import QtQuick 2.3
import QtQuick.Controls 1.2

import "../javascript/Cifras.js" as Cifras

Rectangle {

    id: root
    color: "transparent"
    radius: 4

    signal scfinalizado (int valExacto)

    property bool activo: false
    property bool todasSacadas: false //se activa cuando tenemos las 6 cartas escogidas
    property int espacio: root.width/50
    property int tamañoCuadro : root.width/4
    property bool enFuncionamiento: true
    //señales
    signal cifraSacada(int value)

    GeneradorNumeros {
        id: genNum
        height: tamañoCuadro*0.85
        width: root.width/2.5
        anchors.horizontalCenter: root.horizontalCenter
        anchors.verticalCenter:  root.verticalCenter
        activo: false
        onFinalizado: {
            enFuncionamiento = false
            scfinalizado(exacto);
        }
    }

    Timer {
        id: timer
        interval: 380
        running: false
        repeat: todasSacadas ? false : true
        onTriggered: cifraSacada(Cifras.getCifraPartida());
    }

    onTodasSacadasChanged: if (todasSacadas) genNum.activo = true;

// Funciones ----------------------------------------------------------

    function ejecutar()
    {
        timer.running = true;
    }

    //reinicia el SacaCifras al estado inicial
    function reset()
    {
        genNum.reset()
        activo = true;
        Cifras.reset();
    }
}
