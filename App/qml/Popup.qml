import QtQuick 2.3
import QtQuick.Controls 1.2

//Elemento que consiste en un lanzador que muestra una serie de botones
//segun el numero de elementos del modelo pasado como propiedad
//
// arguments
//  @modelo: modelo con los textos, imagenes y numero de cada elemento del popup
//
// Ejemplo elemento modelo:
//
//  ListElement {
//    numeroElemento: 1
//    fuenteImagen: "qrc:/images/images/home.png"
//    textoBoton: "Home"
//  }
//
// Altura ajustable segun padre .

Item {
    id: root

    //Propiedades obligatorias
    property ListModel modelo
    property bool masAbajo: false
    width: parent.width
    height: parent.height
    smooth: true
    state: "invisible"
    //

    signal elementoPulsado(int index)
    property bool activo: false

    //la ponemos bajo el gridview y el boton para que no afecte a sus clicks
    //Pulsando fuera cierro el popup
    MouseArea
    {
        id: areaFuera
        anchors.fill: parent
        onClicked: {
            if (root.state == "visible")
                root.state = "invisible"
            else
                root.state = "visible"
        }
    }

    BotonImagen{
        id: lanzador

        visible: true
        width: grid.width/5
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: width/1.5
        anchors.topMargin: (masAbajo) ? width*1.5 : width/1.5
        height: width
        imagePath: "qrc:/images/images/grideditor.png"
        onPulsado: {
            if (root.state == "visible")
                root.state = "invisible"
            else
                root.state = "visible"
        }
    }

    Item {
        id: desplegado

        anchors.left: lanzador.left
        anchors.top: lanzador.bottom
        width: root.width
        height: root.height
        visible: false

        Rectangle {
            id: fondoDesplegado
            anchors.fill: grid
            radius: 4
            color: "#333333"
            opacity: 0.7
        }

        GridView
        {
            id: grid
            model: modelo
            width: parent.width*0.5
            cellWidth: width/2
            cellHeight: cellWidth
            height: cellHeight*(modelo.count/2)
            interactive: false

            delegate:
                DelegatePopup
                {
                    texto: textoBoton
                    rutaImagen: fuenteImagen
                    numOperacion: numeroElemento
                    height: grid.cellHeight
                    width: grid.cellWidth
                    onOperacion: elementoPulsado(numOperacion)
                }
        }
    }

    transitions: Transition {

        NumberAnimation { target: desplegado; properties: "opacity"; duration: 300 }
//        NumberAnimation { target: lanzador; properties: "opacity"; duration: 300 }
    }

    states: [

        State {
            name: "invisible"
            PropertyChanges {
                target: desplegado
                opacity: 0
                visible: false
            }
            PropertyChanges {
                target: areaFuera
                enabled: false
            }
        },
        State {
            name: "visible"
            PropertyChanges {
                target: desplegado
                visible: true
                opacity: 1
            }
            PropertyChanges {
                target: areaFuera
                enabled: true
            }
        }
    ]

    function cambiaEstado() {
        if (activo)
        {
            if (state == "visible")
                state = "invisible"
            else
                state = "visible"
        }
        else
            state = "invisible"
    }

    //el indice comienza en 0 y va de izquierda a derecha y de arriba a abajo
    function cambiarIcono(index,rutaIcono)
    {
        grid.currentIndex = index
        grid.currentItem.rutaImagen = rutaIcono
    }

    function cambiarTexto(index,text)
    {
        grid.currentIndex = index
        grid.currentItem.texto = text
    }
}

