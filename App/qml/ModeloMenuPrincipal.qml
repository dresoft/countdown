import QtQuick 2.3
import QtQuick.Controls 1.2

ListModel {
    ListElement {
        texto:"Numbers"
        fuenteImagen:"qrc:/images/images/numbers.png"
        qml:"PantallaCifras.qml"
    }
    ListElement {
        texto:"Letters"
        fuenteImagen:"qrc:/images/images/letters.png"
        qml:"PantallaLetras.qml"
    }
    ListElement {
        texto:"Countdown"
        fuenteImagen:"qrc:/images/images/partida.png"
        qml:"PantallaPartidaCompleta.qml"
    }
    ListElement {
        texto:"Records"
        fuenteImagen:"qrc:/images/images/trofeo.png"
        qml:"PantallaRecords.qml"
    }
}

