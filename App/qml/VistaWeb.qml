import QtQuick 2.3
import QtWebView 1.1

import "../javascript/ConfiguracionGeneral.js" as Conf

Item {

    id: rootWeb
    property string palabra

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "gray"
    }

    Rectangle {
        id: carga
        color: "red"

        anchors.top: parent.top
        anchors.left: parent.left
        height: parent.height*0.02
        width: 0
    }

    Item {

        id: web
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: carga.bottom
        height: parent.height*0.88
        width: parent.width
        enabled: false

        WebView {
            id: pagina
            anchors.fill: parent
            enabled: parent.enabled
            onLoadProgressChanged: {
                console.log("cargado el "+loadProgress+ "% de "+url )
                carga.width = rootWeb.width * (loadProgress/100)

                if (loadProgress == 100)
                    carga.color = "green"
                else
                    carga.color = "red"
                prepararPagina()
            }

            onLoadingChanged: if (WebView.LoadSucceededStatus)
                              {
                                  //Activamos la webview cuando esté cargada
                                  if (loadProgress == 100)
                                  {
//                                     getSource()
                                     web.enabled = true
                                  }
                                  else
                                     web.enabled = false
                              }

            onUrlChanged: {
                var prueba = url.toString()
                //console.log("PAGINA A ABRIR = "+prueba)
                if ((prueba.substring(0,Conf.webDiccionario.length) != Conf.webDiccionario) && (prueba.trim() != ""))
                {
                    if (prueba.substring(0,Conf.webError.length) == Conf.webError)
                    {
                        console.log("Palabra no existe")
                    }
                    else
                    {
                        rootWeb.visible = false
                        console.log("Cerramos por intentar acceder a otra pagina")
                    }
                }
            }

        }

        onEnabledChanged:  console.log("CAMBIAMOS A WEB.ENABLED = " + web.enabled)
    }

    Rectangle {

        id: cierre
        anchors.top: web.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height-web.height-carga.height

        BotonImagen {
            height: parent.height
            width: height
            anchors.centerIn: parent
            imagePath: "qrc:/images/images/cerrar.png"
            onPulsado: rootWeb.visible = false
        }
    }

    onVisibleChanged: {
                if (!visible) { web.enabled = false; pagina.stop()}
    }


    function buscarPalabra(PalabraABuscar)
    {
        rootWeb.visible = true
        var dir = Conf.webDiccionario + PalabraABuscar
        console.log("VistaWeb cargamos la direccion: "+ dir )
        pagina.url = dir
    }

    //--- Funciones -----------------------------------

    function getSource(){
        var js = "document.documentElement.outerHTML";
        pagina.runJavaScript(js, function(result){console.log(result);})
    }

    function scrollToPosition(){
        var js = "window.scrollTo(500, 200)";
        pagina.runJavaScript(js, function(result){console.log(result);})
    }

    //Scrolleamos hasta encontrar el elemento que deseamos
    function scrollToElement(){
        var js = "var myDiv = document.getElementsByTagName('h2')[0];scrollTo(document.body, myDiv.offsetTop, 100);"
        //var js = "var myDiv = document.getElementsByClassName('main-content')[0];scrollTo(document.body, myDiv.offsetTop, 100);"
        pagina.runJavaScript(js, function(result){console.log(result);})
    }

    //Ocultamos el header
    function hideHeaderAndFooter()
    {
        var js = "var myDiv = document.getElementsByTagName('header')[0];myDiv.style.display = 'none';"
        pagina.runJavaScript(js,function(result){console.log(result);})
        var js2 = "var myDiv = document.getElementsByTagName('footer')[0];myDiv.style.display = 'none';"
        pagina.runJavaScript(js2,function(result){console.log(result);})
        var js3 = "var myDiv = document.getElementsByClassName('violbox h-box lex')[0];myDiv.container.feature { display:none !important; }"
        pagina.runJavaScript(js3,function(result){console.log(result);})
    }

    //No usada
    function hideAds()
    {
        var js = "var appBanners = document.getElementsByClassName('violbox h-box lex');for (var i = 0; i < appBanners.length; i ++) {appBanners[i].style.display = 'none !important';}"
        pagina.runJavaScript(js,function(result){console.log(result);})
    }

    //Todas las funciones que deseamos ejecutar para situar la pagina donde queremos
    function prepararPagina()
    {
        console.log("PREPARAMOOOOOOOOOOOOOS")
        //Ocultamos el header y scrolleamos hasta la definicion
        hideHeaderAndFooter()
        scrollToElement()
    }
}
