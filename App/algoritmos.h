#ifndef ALGORITMOS_H
#define ALGORITMOS_H

#include <QObject>
#include <QStringList>
#include <QPair>

#define N 6                     // Numero de cifras con las que se juega
#define NOD_MAXIMO 2400         // Maxima cantidad de nodos generados posibles a partir de 6 elementos
#define SI  1
#define NO  0
#define NULO '\0'

#define SUMA      0
#define RESTA     1
#define MULTIPLICACION  2
#define DIVISION  3

#define SEPARADOR ","

#define distancia(a,b) ( (a)>(b) ? ((a)-(b)) : ((b)-(a)) )
#define copiarCadena(d,o) d = o
#define Cad_Une(d,o) strcat((d),(o))

class Algoritmos : public QObject
{
    Q_OBJECT

    typedef short int      contador;
    typedef int        *nodo;

private:

    int exacto_;
    int mejor_;           // Numero mas cercano al exacto encontrado por ahora
    QString mejorCamino_; // Operaciones a realizar para obtener el valor mas cercano al exacto
    int diferencia_;      // Diferencia entre exacto_ y Mejor
    contador numTotalNodos_;     // Numero total de nodos generados
    contador numNodosVistos_;    // Numero de nodos distintos almacenados
        //Estos contadores toman el valor inicial -1 y el valor 0 ya indica que hay un nodo

    int comprobado[NOD_MAXIMO][N-2];       // Para almacenar los nodos comprobados
    int pilaNumeros[N];      //  Almacena los numeros

public:

    Algoritmos(QObject *parent = 0);

    QPair<int,QStringList>  ejecutarBusqueda(QList<int> lista,int meta);
    bool busca(int L[N],QString C[N]);                   // busca la solucion del problema
    bool buscaEnDos(int L[N],QString C[N]);              // busca la solucion del problema en los nodos con dos elementos
    void cambiaMejor(int Nuevo, QString NuevoCamino);    // Almacena la solucion mas cercana al exacto
    QString concatenarCamino(QString &destino, QString Primero, QString Operacion, QString Segundo); // Une dos caminos con una operacion
    nodo ordenarNodo(int L[N]);     // Ordena un nodo
    bool perteneceANodo(nodo L);    // Devuelve true si un nodo pertenece a otro nodo
    void insertarNodo(nodo L);      // Anota un nodo
    QStringList muestraOperaciones(QString expresion); //Muestra la lista de operaciones realizadas a partir de un camino dado

    QString cadTrozo(QString destino, QString origen, contador comienzo, contador fin);                // Extrae un QString de otro

    inline bool esOperacion(QString c)
    {
        if ((c=="+")||(c=="-")||(c=="*")||(c=="/"))
            return true;
        else return false;
    };

    inline bool esOperacion(QChar c)
    {
        if ((c=='+')||(c=='-')||(c=='*')||(c=='/'))
            return true;
        else return false;
    };

    //OPERACIONES CALCULO -------------------------------------------
    void iniciarCalculadora();       //Inicia la calculadora
    int operar(QChar Operacion);     //Operacion
    int insertar(int Num);           //Inserta un numero
    int calcular(int val1, int val2, QString op);
};

#endif // ALGORITMOS_H
