#include "hilocargadiccionario.h"
#include "diccionario.h"
#include <QTime>
#include <QDebug>

HiloCargaDiccionario::HiloCargaDiccionario()
{

}

void HiloCargaDiccionario::run()
{
    QTime tiempoCarga;
    tiempoCarga.start();
    Diccionario::getInstance();
    qDebug() << "TIEMPO DE CARGA TOTAL DEL DICCIONARIO ==> " << tiempoCarga.elapsed() << " ms";
}
